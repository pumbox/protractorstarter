# Protractor starter test

###Stack
- javasctipt (ECMAScript 6)
- nodejs (JavaScript runtime environment)
- protractor (test framework for AngularJS applications)
- jasmine (behavior-driven test framework)

###How 2 install
It's a nodejs program.  To run, you will need to have Node.js installed.  
Run following commands in project's root folder
- npm install -g protractor *installs protractor globally*
- webdriver-manager update *downloads selelenium binaries*
- npm install *downloads required dependencies*
- createFolders.sh *creates folders for reporter*

###How 2 run
There are 2 options available: One with control flow enabled and another with disabled protractor's CF.
- To run tests without Control Flow  execute ~ # e2e/protractor !CFConf.js
- To run tests with Control Flow  execute  ~ # e2e/protractor conf.js

###References
- protractor style guide: http://www.protractortest.org/#/style-guide
- JavaScript documentation: https://developer.mozilla.org/en-US/docs/Web/JavaScript
- JavaScript tutorial for beginners: https://www.w3schools.com/js/
- NPM: https://docs.npmjs.com/
- Jasmine introduction: https://jasmine.github.io/2.0/introduction.html
- Jasmine Asynchronous Support: https://jasmine.github.io/2.0/introduction.html#section-Asynchronous_Support
- Protractor: http://www.protractortest.org/#/
- Protractor api:  http://www.protractortest.o
- promise vs callback: https://www.quora.com/Whats-the-difference-between-a-promise-and-a-callback-in-Javascript
- JavaScript Shorthand Coding: https://www.sitepoint.com/shorthand-javascript-techniques/
- Tasks, microtasks, queues and schedules: https://jakearchibald.com/2015/tasks-microtasks-queues-and-schedules/
- Promises puzzle: https://twitter.com/nolanlawson/status/578948854411878400
- Promises puzzle answer sheet: https://gist.github.com/nolanlawson/940d1a390b2d9cf9483c
- How to wait to load the element in browser: https://github.com/angular/protractor/issues/1046
- browser.sleep() blocks browser JavaScript from executing?: https://github.com/angular/protractor/issues/2677
- Protractor does not wait for app load after button click: https://github.com/angular/protractor/issues/2358
- Error while waiting for Protractor to sync with the page: true: https://github.com/angular/protractor/issues/4336
- How to implement intervals/polling in angular2 to work with protractor: https://github.com/angular/protractor/issues/3349
- Executing Raw Javascript in Protractor: http://blog.ng-book.com/executing-raw-javascript-in-protractor/
- Xpath vs CSS Selectors: http://www.sinaru.com/2014/12/08/xpath-vs-css-selectors/



###Important Items
- Control flow will be deprecated: https://github.com/SeleniumHQ/selenium/issues/2969  
- What is control flow?: https://github.com/SeleniumHQ/selenium/wiki/WebDriverJs#control-flows
- Switching to async/Await: https://github.com/angular/protractor/tree/5.2.0/exampleTypescript/asyncAwait
