var JasmineReporters = require('jasmine-reporters'),
    HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');

exports.config = {

  seleniumAddress: 'http://localhost:4444/wd/hub',

  SELENIUM_PROMISE_MANAGER: false,

  specs: ['./specs/starterTest.spec.js'],

  baseUrl: 'https://angularjs.org/',

  restartBrowserBetweenTests: true,

  allScriptsTimeout: 300000,

  onPrepare: function() {
    jasmine.getEnv().addReporter(new JasmineReporters.JUnitXmlReporter({
      consolidateAll: true,
      savePath: 'testresults',
      filePrefix: 'results',
      reportOnlyFailedSpecs: false,
      captureOnlyFailedSpecs: false
    }));
    jasmine.getEnv().addReporter(
        new HtmlScreenshotReporter({
          dest: 'testresults',
          filename: 'my-report.html'
        })
    )
  },
  capabilities:{
    'browserName': 'chrome',
    loggingPrefs: {
      'driver': 'DEBUG',
      'browser': 'DEBUG'
    }
  }
};
