'use strict';

let AbstractPage = require('./abstract.page').AbstractPage,
    uiMap = require('../page/ui.map.js');

function frontPage() {
  AbstractPage.call(this);
  let self = this;
  self.addNewItem = addNewItem;
  self.isItemInList = isItemInList;

  function addNewItem(itemName) {
    let input = self.findElementByCss(uiMap.css.frontPage.todo.input),
        addButton = self.findElementByCss(uiMap.css.frontPage.todo.addButton),
        expectedItem = self.findElementByXpath(uiMap.xpath.frontPage.todo.listItem(itemName));
    self.waitForElementAndTypeInto(input, itemName);
    self.waitForElementToBeClickableAndClick(addButton);
    self.waitForElementToBeVisible(expectedItem);
  }
  function isItemInList(itemName) {
    let expectedItem = self.findElementByXpath(uiMap.xpath.frontPage.todo.listItem(itemName));
    return expectedItem.getText().then((text)=>{
      return text
    },err=>{throw err})

  }
}

module.exports = frontPage;