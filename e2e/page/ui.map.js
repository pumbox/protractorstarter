let xpath = {},
    css = {};

css.frontPage = {};
css.frontPage.theBasics = '#the-basics';
css.frontPage.addSomeControl = '#add-some-control';
css.frontPage.wireUpBackend = '#wire-up-a-backend';
css.frontPage.pageLoadedCheck = [css.frontPage.theBasics, css.frontPage.addSomeControl, css.frontPage.wireUpBackend];

css.frontPage.basic = {};
css.frontPage.basic.input = "[app-run = 'hello.html'] input";
css.frontPage.basic.output = "[app-run = 'hello.html'] h1";
css.frontPage.todo = {};
css.frontPage.todo.input = "[app-run = 'todo.html'] input[type=text]";
css.frontPage.todo.output = "[app-run = 'hello.html'] h1";
css.frontPage.todo.addButton = "[app-run = 'todo.html'] input[type=submit]";

xpath.frontPage = {};
xpath.frontPage.todo = {};
xpath.frontPage.todo.listItem = (itemName) => {return "//*[@app-run = 'todo.html']//span[text() = '"+ itemName +"']"}; //css doesn't support searching elements by text.


module.exports = {
  xpath: xpath,
  css:css,
};
