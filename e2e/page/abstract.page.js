'use strict';

let EC = protractor.ExpectedConditions;

function AbstractPage() {
  let self = this;

  self.config = {};
  self.config.waitTime = 10000;

  // abstract page interface
  self.findElementByCss = findElementByCss;
  self.findElementsByCss = findElementsByCss;
  self.findElementByXpath = findElementByXpath;
  self.findElementsByXpath = findElementsByXpath;
  self.get = get;
  self.wait = wait;
  self.getAttribute = getAttribute;
  self.click = click;
  self.clear = clear;
  self.typeInto = typeInto;
  self.clearAndTypeInto = clearAndTypeInto;
  self.waitForElementToBeClickable = waitForElementToBeClickable;
  self.waitForElementToBeVisible = waitForElementToBeVisible;
  self.waitForElementAndTypeInto = waitForElementAndTypeInto;
  self.waitForElementClearAndTypeInto = waitForElementClearAndTypeInto;
  self.waitForElementToBeClickableAndClick = waitForElementToBeClickableAndClick;

  function findElementByCss(css) {
    return element(by.css(css));
  }
  function findElementsByCss(css) {
    return element.all(by.css(css));
  }
  function findElementByXpath(xpath) {
    return element(by.xpath(xpath))
  }
  function findElementsByXpath(xpath) {
    return element.all(by.xpath(xpath));
  }
  function get(url) {
    browser.get(url).then(ok => {return ok
    },err => {throw 'An error occurred during get page: ' +err});
  }
  function wait(timeout) {
    return new Promise(function (success) {
      setTimeout(()=>{
        success()
      }, timeout)
    })
  }
  function getAttribute(element, attribute) {
    return element.getAttribute(attribute);
  }
  function click(element) {
    return element.click().then(ok => {return ok
    },err => {throw 'An error occurred during field element click: ' + err});
  }
  function clear(element) {
    return element.clear().then(ok => {return ok
    },err => {throw 'An error occurred during element clear: ' + err});
  }
  function typeInto(element, value) {
    return element.sendKeys(value).then(ok => {return ok
    },err => {throw 'An error occurred during typeInto element : ' + err});
  }
  function clearAndTypeInto(element, value) {
    return self.clear(element).then(() => {
      return self.typeInto(element, value);
    });
  }
  function waitForElementToBeClickable(element,alternativeWaitTime) {
    if (alternativeWaitTime) {
      return browser.wait(EC.elementToBeClickable(element), alternativeWaitTime, 'An error occurred during wait for element to be clickable.');
    } else {
      return browser.wait(EC.elementToBeClickable(element), self.config.waitTime, 'An error occurred during wait for element to be clickable.');
    }
  }
  function waitForElementToBeVisible(element, alternativeWaitTime) {
    //console.log('Started waiting for element to be visible');
    if (alternativeWaitTime) {
      return browser.wait(EC.visibilityOf(element), alternativeWaitTime, 'An error occurred during wait for element to be visible.')
    } else{
      return browser.wait(EC.visibilityOf(element), self.config.waitTime, 'An error occurred during wait for element to be visible.')
    }
  }
  function waitForElementAndTypeInto(element, value, alternativeWaitTime) {
    return self.waitForElementToBeClickable(element, alternativeWaitTime).then(() => {
      return self.typeInto(element, value)
    });
  }
  function waitForElementClearAndTypeInto(element, value, alternativeWaitTime) {
    return self.waitForElementToBeClickable(element, alternativeWaitTime).then(() => {
      return self.clearAndTypeInto(element, value)
    });
  }
  function waitForElementToBeClickableAndClick(element,alternativeWaitTime) {
    //console.log('Started waitForElementToBeClickableAndClick');
    if (alternativeWaitTime) {
      return self.waitForElementToBeClickable(element,alternativeWaitTime).then(() => {
        return element.click()});
    } else {
      return self.waitForElementToBeClickable(element,self.config.waitTime).then(() => {
        return element.click();
      });
    }
  }
}
module.exports = {
  AbstractPage: AbstractPage
};
