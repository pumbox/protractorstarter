'use strict';
let EC = protractor.ExpectedConditions,
    uiMap = require('./page/ui.map.js');

function Browser() {
  let self = this,
      CF = browser.controlFlowIsEnabled(),
      appLoadTimeout = 10*1000,
      startLoad,
      endLoad,
      loadResult = {};
  self.checkElementLoaded = checkElementLoaded;

  this.waitForAppToLoad = () => {
    startLoad = new Date();
    this.setDefaultSize();
    //console.log('Start browser init: ' + new Date().toISOString());
    return new Promise ((success, reject)=> {
      browser.get(browser.baseUrl).then(()=> {
        //console.log('get finished '+ new Date().toISOString());
        browser.waitForAngular().then(()=>{
          //console.log('Angular: zaladowalemsie '+ new Date().toISOString());
          self.ignoreSynchronization();
          let allElements = [];
          uiMap.css.frontPage.pageLoadedCheck.forEach((one) => {
            allElements.push(self.checkElementLoaded(one))
          });
          Promise.all(allElements).then(() => {
            endLoad = new Date();
            loadResult.loadTime = millisToMinutesAndSeconds(endLoad - startLoad);
            //console.log('OK all elements are visible '+ new Date().toISOString());
            loadResult.status = true;
            success(loadResult)
          },(err) => {
            reject('Failure during app loaded check. ' + err)
          })
        },err=>{throw 'Failure during app loaded check. Angular fail to load ' + err})
      })
    })
  };
  this.ignoreSynchronization = () => {
      browser.ignoreSynchronization = true;
      return this;
    };
  this.executeScript = script => {
    return browser.executeScript(script);
  };
  this.disableAnimations = ()=> {
    var style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = '* {' +
        '-webkit-transition: none !important;' +
        '-moz-transition: none !important;' +
        '-o-transition: none !important;' +
        '-ms-transition: none !important;' +
        'transition: none !important;' +
        'transform: none !important;' +
        '}';
    document.getElementsByTagName('head')[0].appendChild(style);
  };
  this.setDefaultSize = function () {
    browser.driver.manage().window().setSize(1100, 1000);
    return this;
  };
  function checkElementLoaded(elem) {
    let item = element({css:elem}),
        searchInterval = 200;
    if(CF){
      return browser.wait(EC.presenceOf(item), appLoadTimeout)
    }
    else{
      return new Promise((success, reject) => {
        let circle;
        circle = setInterval(function () {
          item.isDisplayed().then((result) => {
            if(result){
              //console.log('Element was found: ' + result);
              clearInterval(circle);
              circle = null;
              success(true)
            }
          }, err => {
            reject(err)
          });
        }, searchInterval);
        setTimeout(() => {
          if(circle){
            clearInterval(circle);
            reject('Err in app load check. No element found within specified time frame.')
          }
        }, appLoadTimeout);
      })
    }
  };
  function millisToMinutesAndSeconds(millis) {
    let minutes = Math.floor(millis / 60000),
        seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
  }
}
module.exports = Browser;