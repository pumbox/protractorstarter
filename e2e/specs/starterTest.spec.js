'use strict';
let uiMap = require('../page/ui.map.js'),
    FrontPage = require('../page/frontPage.js'),
    Browser = require('../browser');

describe('', () => {
  let frontPage;

  beforeEach((done)=> {
    let browser = new Browser();
    browser.waitForAppToLoad().then((loadTime) => {
      //console.log('App. load time: ' + loadTime.loadTime + ' (m:ss)');
      frontPage = new FrontPage();
      done()
    },err=>{throw err});
  });

  it(`Should test Basic Angular example`, (done) => {
    // Basic test which changes a text in simple angular app. and then fetches the text and compares with expected value.
    // 'done' callback indicates that test is finished. This is important for assync app testing. For more details see the readme file (Jasmine Asynchronous Support).
      let input = frontPage.findElementByCss(uiMap.css.frontPage.basic.input),
          output = frontPage.findElementByCss(uiMap.css.frontPage.basic.output),
          expectedText = 'world';
      frontPage.waitForElementAndTypeInto(input, expectedText);
      output.getText().then(function (text) {
        expect(text).toContain(expectedText);
        done()
      })
  });
  it(`Should test Todo Angular example V1`, (done) => {
    //The test adds new item list and checks whether the item is in list of items.
    //xpath selector is used here as css doesn't support searching elements by text.
    //The test also shows how control flow handles call queue. You can write async code as sync using control flow. (For more details see. readme What is control flow?)
      let input = frontPage.findElementByCss(uiMap.css.frontPage.todo.input),
          addButton = frontPage.findElementByCss(uiMap.css.frontPage.todo.addButton),
          expectedText = 'New item',
          expectedItem = frontPage.findElementByXpath(uiMap.xpath.frontPage.todo.listItem(expectedText));
      frontPage.waitForElementAndTypeInto(input, expectedText);
      frontPage.waitForElementToBeClickableAndClick(addButton);
      frontPage.waitForElementToBeVisible(expectedItem);
      expectedItem.getText().then((text)=>{
        expect(text).toContain(expectedText);
        done()
      })
  });
  it(`Should test Todo Angular example V2`, () => {
      //The same test as above but written in a bit another way. All actions are hidden in page (frontPage.js) file. Also there is no 'done' callback, which shows how protractor's control flow handles command calls.
      //This test should fail in case Control Flow is disabled. See readme file for more details about control flow.
      let newItemText = 'New item',
          expectedResult;
      frontPage.addNewItem(newItemText);
      expectedResult = frontPage.isItemInList(newItemText);
      expect(expectedResult).toContain(newItemText)
  });
});


